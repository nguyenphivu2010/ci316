<div class="col-md-6">
<h1 class="display-4 text-center">Register</h1>
    <?php echo form_open(base_url().('register')); ?>
        <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input type="email" name='email' class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" value="<?php echo(isset($email) ? $email: set_value('email')); ?>" required>
            <?php  echo form_error('email', '<p class=" alert-danger">', '</p>');?>
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" name='password' class="form-control" id="exampleInputPassword1" placeholder="Password" value="<?php if(isset($password)){
                echo $password;
            } ?>" required>
            <?php  echo form_error('password', '<p class=" alert-danger">', '</p>');?>
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Address</label>
            <input type="text" name='address' class="form-control" placeholder="Enter your address" value="<?php echo(isset($address) ? $address: set_value('address')); ?>" required><?php  echo form_error('address', '<p class=" alert-danger">', '</p>');?>
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Full Name</label>
            <input type="text" name='username' class="form-control" placeholder="Enter your username" value="<?php echo(isset($username) ? $username: set_value('username')); ?>" required>
            <?php  echo form_error('username', '<p class=" alert-danger">', '</p>');?>
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Phone</label>
            <input type="text" name='phone' class="form-control" placeholder="Enter your phone" value="<?php echo(isset($phone) ? $phone: set_value('phone')); ?>" required>
            <?php  echo form_error('phone', '<p class=" alert-danger">', '</p>');?>
        </div>
        <?php if($this->session->flashdata('reportE')){
         echo "<div class='alert alert-warning' ><p>".$this->session->flashdata('report')."</p></div>";}
         if($this->session->flashdata('reportS')){
            echo "<div class='alert alert-success' ><p>".$this->session->flashdata('reportS')."</p></div>";} ?>
        <button type="submit" name='submit' value='submit' class=" btn btn-info float-right">Submit</button>
    </form>
</div>        