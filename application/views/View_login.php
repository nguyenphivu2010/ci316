<div class="col-md-4">
<h1 class="display-4 text-center">Login</h1>
    <!-- <form action='/login/ActionLogin' method='POST'> -->
    <?php echo form_open(base_url().('login')); ?>
        <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input type="email" name='email' class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" value="
            <?php echo(set_value('email')); ?>" required>
            <?php  echo form_error('email', '<p class=" alert-danger">', '</p>');?>
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" name='password' class="form-control" id="exampleInputPassword1" placeholder="Password" value="<?php if(isset($password)){
                echo $password;
            } ?>" required>
            <?php  echo form_error('password', '<p class=" alert-danger">', '</p>');?>
        </div>
        <div class="form-check">
        <label class="form-check-label">
            <input type="checkbox" class="form-check-input" 
            <?php if(isset($password)){
                echo 'checked';
            } ?>
            name='remember'>
            Remenber me
        </label>
        </div>
        <?php if($this->session->flashdata('report')){
         echo "<div class='alert alert-warning' ><p>".$this->session->flashdata('report')."</p></div>";} ?> 
        <button type="submit" name='submit' value='submit' class=" btn btn-primary float-right">Submit</button>
    </form>
</div>        