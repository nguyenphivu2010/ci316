<?php
namespace CI316\hooks;

class HookMiddleware
{
    protected $CI;
    protected $middleware = [];
    protected $middlewareGroups = [];
    protected $currentMethod;

    public function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->config->load('middleware', true);
        $this->CI->config->load('routes', true);
        $this->CI->json = new \CI316\libraries\Response;
        $this->CI->currentMethod = $this->CI->router->class . '/' . $this->CI->router->method;
        $this->middleware = $this->CI->config->item('middleware')['middleware'] ?? [];
        $this->middlewareGroups = $this->CI->config->item('middleware')['middlewareGroups'] ?? [];
    }

    public function run()
    {
        $config = $this->CI->config->item('routes')['middleware'];

        if ($config) {
            $config($this);
        }
    }

    public function single($name, $routes, $next = null)
    {
        $hook = [
            'middleware' => $name,
            'routes' => $routes,
        ];
        $this->singleMiddleware($hook, $next);
    }

    public function group($name, $routes, $next = null)
    {
        $hook = [
            'group' => $name,
            'routes' => $routes,
        ];
        $this->groupMiddleware($hook, $next);
    }

    protected function groupMiddleware(array $group, callable $function = null)
    {
        if (empty($group)) {
            return;
        }

        if (in_array($this->CI->currentMethod, $group['routes'])) {
            $middlewares = $this->middlewareGroups[$group['group']] ?? [];
            foreach ($middlewares as $middleware) {
                $this->runMiddleware($middleware);
            }
        }

        if (is_callable($function) && $function !== null) {
            return $function($this);
        }
    }

    protected function singleMiddleware(array $hook, callable $function = null)
    {
        if (empty($hook)) {
            return;
        }
        
        if (in_array($this->CI->currentMethod, $hook['routes'])) {
            $this->runMiddleware($hook['middleware']);
        }
        if (is_callable($function) && $function !== null) {
            return $function($this);
        }
    }

    protected function runMiddleware($nameMiddleware)
    {
        $classMiddleware = $this->middleware[$nameMiddleware] ?? null;
        if ($classMiddleware) {
            return (new $classMiddleware)->run();
        }
    }
}
