<?php

namespace CI316\middlewares;

class Admin extends BaseMiddleware
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run()
    {
        $role = $this->CI->session->userdata('role_name');
        if ($role != 'Admin') {
            redirect('/home');
        }
    }
}