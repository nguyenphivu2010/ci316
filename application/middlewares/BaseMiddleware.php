<?php

namespace CI316\middlewares;

use CI316\libraries\Response as JsonResponse;

abstract class BaseMiddleware
{
    protected $CI;

    public function __construct()
    {
        $this->CI = &get_instance();
    }

    abstract public function run();

    public function responseError($data = null, $http_code = 500, $message = '')
    {
        return (new JsonResponse)->response($data, $http_code, $message);
    }
}
