<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$hook['post_controller_constructor'][] = [
    'class' => '\CI316\hooks\HookMiddleware',
    'function' => 'run',
    'filename' => 'HookMiddleware.php',
    'filepath' => 'hooks',
];



