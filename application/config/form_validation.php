<?php
$config = [
    'signup' => array(
        array(
            'field'     => 'email',
            'label'     => 'Email',
            'rules'     => 'trim|required|valid_email',
            'errors'    => array
            (
                'required'      => 'You must enter Email.',
                'valid_email'   => 'Email must be in the correct format abc@xyz.jk'
            )
        ),
        array(
            'field'     => 'password',
            'label'     => 'Password',
            'rules'     => 'trim|required|min_length[5]|max_length[255]',
            'errors'    => array
            (
                'required'      => 'You must enter Password.',
                'min_length'    => 'Password minlenght 5 character.',
                'max_length'    => 'Password maxlenght 255 character.' 
            )
        )
    ),
    'register' => array(
        array(
            'field'     => 'email',
            'label'     => 'Email',
            'rules'     => 'trim|required|valid_email|callback_checkEmailUnique',
            'errors'    => array
            (
                'required'          => 'You must enter Email.',
                'valid_email'       => 'Email must be in the correct format abc@xyz.jk',
            )
        ),
        array(
            'field'     => 'password',
            'label'     => 'Password',
            'rules'     => 'trim|required|min_length[5]|max_length[255]',
            'errors'    => array
            (
                'required'      => 'You must enter Password.',
                'min_length'    => 'Password minlenght 5 character.',
                'max_length'    => 'Password maxlenght 255 character.' 
            )
        ),
        array(
            'field'     => 'address',
            'label'     => 'Address',
            'rules'     => 'trim|required|min_length[5]|max_length[255]',
            'errors'    => array
            (
                'required'      => 'You must enter Address.',
                'min_length'    => 'Address minlenght 5 character.',
                'max_length'    => 'Address maxlenght 255 character.' 
            )
        ),
        array(
            'field'     => 'username',
            'label'     => 'UserName',
            'rules'     => 'trim|required|min_length[5]|max_length[255]',
            'errors'    => array
            (
                'required'      => 'You must enter UserName.',
                'min_length'    => 'UserName minlenght 5 character.',
                'max_length'    => 'UserName maxlenght 255 character.' 
            )
        ),
        array(
            'field'     => 'phone',
            'label'     => 'Phone',
            'rules'     => 'trim|required|min_length[10]|max_length[30]|is_natural',
            'errors'    => array
            (
                'required'      => 'You must enter Phone.',
                'min_length'    => 'Phone minlenght 10 character.',
                'max_length'    => 'Phone maxlenght 255 character.',
                'is_natural'    => 'Phone enter the numberic' 
            )
        ),
    ),
    'editaccount' => array(
        array(
            'field'     => 'email',
            'label'     => 'Email',
            'rules'     => 'trim|required|valid_email',
            'errors'    => array
            (
                'required'      => 'You must enter Email.',
                'valid_email'   => 'Email must be in the correct format abc@xyz.jk'
            )
        ),
        array(
            'field'     => 'address',
            'label'     => 'Address',
            'rules'     => 'trim|required|min_length[5]|max_length[255]',
            'errors'    => array
            (
                'required'      => 'You must enter Address.',
                'min_length'    => 'Address minlenght 5 character.',
                'max_length'    => 'Address maxlenght 255 character.' 
            )
        ),
        array(
            'field'     => 'username',
            'label'     => 'UserName',
            'rules'     => 'trim|required|min_length[5]|max_length[255]',
            'errors'    => array
            (
                'required'      => 'You must enter UserName.',
                'min_length'    => 'UserName minlenght 5 character.',
                'max_length'    => 'UserName maxlenght 255 character.' 
            )
        ),
        array(
            'field'     => 'phone',
            'label'     => 'Phone',
            'rules'     => 'trim|required|min_length[9]|max_length[30]|is_natural',
            'errors'    => array
            (
                'required'      => 'You must enter Phone.',
                'min_length'    => 'Phone minlenght 9 character.',
                'max_length'    => 'Phone maxlenght 255 character.',
                'is_natural'    => 'Phone enter the numberic' 
            )
        ),
    ),
    'search' => array(
        array(
            'field'     => 'search',
            'label'     => 'Data',
            'rules'     => 'trim|required|max_length[255]',
            'errors'    => array
            (
                'required'      => 'You must enter data.',
                'valid_email'   => 'Email must be in the correct format abc@xyz.jk'
            )
        ),
        array(
            'field'     => 'radio',
            'label'     => 'Radio',
            'rules'     => 'trim|required|in_list[0,1]',
            'errors'    => array
            (
                'required'      => 'You must enter Data.',
                'in_list'    => 'Data 0 or 1.',
            )
        )
    ),
];