<?php
use CI316\core\BaseController;
class Account extends BaseController
{
    public function __construct()
    {       
        parent::__construct();
        $this->load->model('Maccount');
        $this->load->helper(array('form','url','cookie'));
        $this->load->library(array('encrypt','session','form_validation'));                
    }
    /*
    *show page account manager
    * 
    */
    public function indexAccount()
    {
        $data['data']=$this->Maccount->listAccount();
        $this->load->templateAdmin('account/list_view', $data);
    }
    /*
    *show page edit account
    * 
    */
    public function showAccountEdit()
    {
        $accid=$this->input->post('accid');
        $data['data']=$this->Maccount->getAccountByID($accid);
        $data['roles']= $this->Maccount->listRoles();
        $this->load->templateAdmin('AccountEdit', $data);
    }
    /*
    *show page profile
    * 
    */
    public function showProfile(){
        $accid=$this->session->userdata('id');
        $data['data']=$this->Maccount->getAccountByID($accid);
        $this->load->templateAdmin('Profile', $data);
    }
    /*
    *show page delete account
    * 
    */
    public function showAccountDelete()
    {
        $accid=$this->input->post('accid');
        $data['data']=$this->Maccount->getAccountByID($accid);        
        $this->load->templateAdmin('AccountDelete', $data);
    }
    /*
    *Accoint edit account
    * 
    */
    public function editAccount()
    {
        $accid=$this->input->post('accid');
        if($this->form_validation->run('editaccount')) {
            $dataPost=[
                'email'     => $this->input->post('email'),
                'address'   => $this->input->post('address'),
                'username'  => $this->input->post('username'),
                'phone'     => $this->input->post('phone'),
                'role_id'   => $this->input->post('role'),
                'updated'   => date('Y-m-d H:i:s')
            ];
            $this->Maccount->updateAccount($accid,$dataPost);
            redirect('/account');
        }
        $this->load->templateAdmin('AccountEdit');
    }
    /*
    *Acction delete account
    * 
    */
    public function deleteAccount()
    {
        $accid=$this->input->post('accid');
        $this->Maccount->deleteAccountByID($accid);
        redirect('/account');
    }
    /*
    *show register account
    * 
    */
    public function showRegister()
    {
        if(!empty(get_cookie('remember'))){
            $data=unserialize($this->my_crypt(get_cookie('remember'), 'd'));
            $result=$this->User->checkLogin($data['email'], $data['password']);
            if(!empty($result)){
                $this->writeSession($result);
                redirect('/home');
            }
        }
        $this->load->template('RegisterV');
    }
    /*
    * Process form login
    */
    public function actionRegister()
    {
        $this->session->set_flashdata('reportS',"");
        if($this->form_validation->run('register')) {
            $dataPost=[
                'email'     => $this->input->post('email'),
                'password'  => $this->input->post('password'),
                'address'   => $this->input->post('address'),
                'username'  => $this->input->post('username'),
                'phone'     => $this->input->post('phone'),
            ];
            $this->checkEmailUnique($dataPost['email']);
            $this->Maccount->insertAccount($dataPost['email'],$dataPost['password'],$dataPost['address'],$dataPost['username'],$dataPost['phone']);
            $this->session->set_flashdata('reportS',"Regiset account success");
        }
        $this->load->template('RegisterV');
    }
    /*
    *Function check email unique
    * 
    */
    public function checkEmailUnique($email)
    {
        $unique=$this->Maccount->checkEmailM($email);        
        if ($unique->num_rows() > 0)
        {
            $this->form_validation->set_message('checkEmailUnique', "The %s field does not have our email.");
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
    /*
    *Function search account
    * 
    */
    public function searchAccount()
    {
        if($this->form_validation->run('search')) {
            $dataPost=[
                'search'     => $this->input->post('search'),
                'radio'  => $this->input->post('radio'),
            ];
            $data['data']=$this->Maccount->searchAccount($dataPost['search'], $dataPost['radio']);
        }
        else{
             $data['data']=$this->Maccount->listAccount();
        }
        $this->load->templateAdmin('account/list_view', $data);
    }
}