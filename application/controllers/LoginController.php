<?php
use CI316\core\BaseController;
class LoginController extends BaseController {
    public function __construct(){
        parent::__construct();
        $this->load->library(array('encrypt','session','form_validation'));        
        $this->load->helper(array('form','url','cookie'));
        $this->load->model('User');
    }
    /*
    * Show view_login
    */
    public function showLogin()
    {
        if(!empty(get_cookie('remember'))){
            $data=unserialize($this->my_crypt(get_cookie('remember'), 'd'));
            $result=$this->User->checkLogin($data['email'], $data['password']);
            if(!empty($result)){
                $this->writeSession($result);
                redirect('/home');
            }
        }
        $this->load->template('View_login');
    }
    /*
    * Process form login
    */
    public function actionLogin()
    {
        $this->session->set_flashdata('report','');
        if($this->form_validation->run('signup')) {
            $dataPost=[
                'email' => $this->input->post('email'),
                'password'  => $this->input->post('password')
            ];
            $result=$this->User->checkLogin($dataPost['email'], $dataPost['password']);
            if(!empty($result)){
                $this->writeSession($result);
                if($this->input->post('remember') == TRUE ){
                    $this->writeCookies($dataPost);
                }
                $last_page=$this->session->userdata('last_page');
                isset($last_page) ? redirect($last_page) :redirect('/home');
            }
            $this->session->set_flashdata('report',"Email or Password is incorrect!");
        }
        $this->load->template('View_login');
    }
    /*
    * Process logout page
    */
    public function logout(){
        $this->session->sess_destroy(["account",'id']);
        delete_cookie('remember');
        redirect('/login');
    }
    /*
    * Set session login
    */
    public function writeSession($result)
    {
        $user = [
            'id'  => $result->accid,
            'email'     => $result->email,
            'username'     => $result->username,
            'role_name'     => $result->role_name,
        ];
        $this->session->set_userdata($user);
    }
    /*
    * Set cookie(email, pass) login
    */
    public function writeCookies($dataPost)
    {
        $data=serialize([
            'email' => $dataPost['email'],
            'password' => $dataPost['password']
        ]);
        $token = $this->my_crypt($data, 'e');
        $cookieRM = array(
            'name'   => 'remember',
            'value'  => $token,
            'expire' => time()+(60 * 60 * 24 * 30)
        );
        set_cookie($cookieRM);
    }
}