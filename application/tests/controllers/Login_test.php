<?php
class Login_test extends TestCase
{
    public function setUP(){
        $this->resetInstance();
        $rows = [
            'email'       => 'admin@neo-lab.vn',
            'password'    => md5('admin'),
            'role_id'     => 1
        ];
        $this->CI->db->truncate('accounts');
        $this->CI->db->insert('accounts',$rows);
    }
    public function testShowLogin()
	{
        $output = $this->request('GET', '/login');
        $this->assertResponseCode(200);
        $this->assertContains('<form action="'.base_url().'login" method="post" accept-charset="utf-8">', $output);
    }
    public function testLoginValidEmailError()
    {
        $data=[
            'email'     =>  'admin@neo-lab',
            'password'  =>  'asdasd',
        ];
        $reports = 'Email must be in the correct format abc@xyz.jk';
        $output = $this->request('POST', '/login', $data);
        $this->assertContains($reports, $output);
        
    }
    public function testLoginValidPassError()
    {
        $data=[
            'email'     =>  'admin@neo-lab.vn',
            'password'  =>  'asc',
        ];
        $reports = 'Password minlenght 5 character.';
        $output = $this->request('POST', '/login', $data);
        $this->assertContains($reports, $output);
        
    }
    public function testEmailBlank()
    {
        $data=[
            'email'     =>  '',
            'password'  =>  'asdasd',
        ];
        $reports = 'You must enter Email.';
        $output = $this->request('POST', '/login', $data);
        $this->assertContains($reports, $output);
        
    }
    public function testPassBlank()
    {
        $data=[
            'email'     =>  'admin@neo-lab.vn',
            'password'  =>  '',
        ];
        $reports = 'You must enter Password.';
        $output = $this->request('POST', '/login', $data);
        $this->assertContains($reports, $output);
        
    }
    public function testEmailPassIncorrect()
    {
        $data=[
            'email'     =>  'asas@neo-lab.vn',
            'password'  =>  'admasain',
        ];
        $reports = 'Email or Password is incorrect!';
        $output = $this->request('POST', '/login', $data);
        $this->assertContains($reports, $output);
        
    }
    public function testLoginSuccess()
    {
        $data=[
                'email'     =>  'admin@neo-lab.vn',
                'password'  =>  'admin',
            ];
        $output = $this->request('POST', '/login', $data);
        $this->assertRedirect('/home');
        $this->assertEquals($data['email'], $_SESSION['email']);
    }
    public function testLogout()
    {
        $data=[
            'email'     =>  'admin@neo-lab.vn',
            'password'  =>  'admin',
        ];
        $output = $this->request('POST', '/login', $data);
        $this->assertRedirect('/home');        
        $this->request('GET', '/logout');
        $this->assertRedirect('/login');    
    }
}
