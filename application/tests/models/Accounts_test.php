<?php
class Accounts_test extends TestCase
{
    public function setUp()
    {
        $this->resetInstance();
        $this->CI->load->model('Maccount');      
    }

    public function testCheckListAccount()
    {
        $rows = [
            [
                'email'     => 'admin@neo-lab.vn',
                'password'  => md5('admin'),
                'address'   => 'Da Nang',
                'username'  => 'Phi Vu Nguyen',
                'phone'     => '01225584121',
                'role_id'   =>  '1'
            ],
            [
                'email'     => 'vu.np@neo-lab.vn',
                'password'  => md5('admin'),
                'address'   => 'Da Nang',
                'username'  => 'Phi Vu Nguyen',
                'phone'     => '01225584121',
                'role_id'   =>  '0'
            ]            
        ];
        $this->CI->db->truncate('accounts');
        $this->CI->db->insert_batch('accounts',$rows);  
        $result = $this->CI->Maccount->listAccount();
        foreach ($rows as $key => $value) {
            $this->assertEquals($value['email'], $result[$key]['email']);
        }
    }
    public function testCheckRoles()
    {
        $rows = [
            [
                'id'            => '0',
                'role_name'     => 'User'
            ],
            [
                'id'            => '1',
                'role_name'     => 'Admin',
            ]
        ];
        $result = $this->CI->Maccount->listRoles();
        foreach ($rows as $key => $value) {
            $this->assertEquals($value['role_name'], $result[$key]['role_name']);
        }
    }
    public function testCheckGetID()
    {
        $row =
            [
                'id'        => '1',
                'email'     => 'admin@neo-lab.vn',
                'password'  => md5('admin'),
                'address'   => 'Da Nang',
                'username'  => 'Phi Vu Nguyen',
                'phone'     => '01225584121',
                'role_id'   =>  '1'
            ];
        $result = $this->CI->Maccount->getAccountByID($row['id']);
        $this->assertEquals($row['email'], $result->email);
    }
    public function testUpdateAccount()
    {
        $row =
        [
            'email'     => 'vu.np@neo-lab.vn',
            'address'   => 'Da Nang Viet Nam',
            'username'  => 'Phi Vu Nguyen',
            'phone'     => '01225584121',
        ];
        $result = $this->CI->Maccount->updateAccount('2',$row);
        $this->assertTrue($result);
    }
    public function testDeleteAccount()
    {

        $result = $this->CI->Maccount->deleteAccountByID('2');
        $this->assertTrue($result);
    }
    public function testCheckEmailAccount()
    {
        $data =
        [
            'email'     => 'admin12@neo-lab.vn',
        ];
        $result = $this->CI->Maccount->checkEmailM($data['email']);
        $this->assertTrue(($result->num_rows()==1)? false : true);
    }
    public function testInsertAccount()
    {
        $row =
        [
            'email'     => 'admin12@neo-lab.vn',
            'password'  => md5('admin'),
            'address'   => 'Da Nang',
            'username'  => 'Phi Vu Nguyen',
            'phone'     => '01225584121',
        ];
        $result = $this->CI->Maccount->insertAccount($row['email'],$row['password'],$row['address'],$row['username'],$row['phone']);
        $this->assertTrue($result);
    }
    public function testSearchAccount()
    {
        $row=
        [
            'data'      => 'admin12@neo-lab.vn',
            'radio'     => '1'
        ];
        $result = $this->CI->Maccount->searchAccount($row['data'],$row['radio']);
        $this->assertEquals($row['data'],$result[0]['email']);
    }
    
}