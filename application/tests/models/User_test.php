<?php

class User_test extends TestCase
{
    public function setUp()
    {
        $this->resetInstance();
        $this->CI->load->model('User');
        $rows = [
            'email'     => 'admin@neo-lab.vn',
            'password'  => md5('admin'),
            'address'   => 'Da Nang',
            'username'  => 'Phi Vu Nguyen',
            'phone'     => '01225584121',
            'role_id'   => 1
        ];
        $this->CI->db->truncate('accounts');
        $this->CI->db->insert('accounts',$rows);
    }

    public function testCheckLoginTrue()
    {
        $data = 
            [
                'email'     => 'admin@neo-lab.vn',
                'password'  => 'admin'    
            ];
        $result = $this->CI->User->checkLogin($data['email'], $data['password']);
        $this->assertEquals( $data['email'],$result->email);
    }
    public function testCheckLoginFalse()
    {
        $data = 
            [
                'email'     => 'asdasd@neo-lab.vn',
                'password'  => 'asdasdasd'    
            ];
        $result = $this->CI->User->checkLogin($data['email'], $data['password']);
        $this->assertEmpty($result);
    }
    

}

