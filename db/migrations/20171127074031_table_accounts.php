<?php


use Phinx\Migration\AbstractMigration;

class TableAccounts extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up(){
        $this->table('accounts', ['id' => false, 'primary_key' => ['id'], 'engine' => 'InnoDB', 'collation' => 'utf8mb4_unicode_ci'])
        ->addColumn('id', 'biginteger', ['limit' => 20, 'signed' => false, 'identity' => true])
            ->addIndex(['email'], ['unique' => true])
            ->addColumn('password', 'string')
            ->addColumn('address', 'string')
            ->addColumn('username', 'string')
            ->addColumn('phone', 'string')
            ->addColumn('role_id', 'integer')         
            ->save();
    }
    public function down()
    {

    }

}
