<?php


use Phinx\Seed\AbstractSeed;

class AddRowsAccount extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $rows = [
            [
                'email'       => 'admin@neo-lab.vn',
                'password'    => '21232f297a57a5a743894a0e4a801fc3',
                'address'     => 'Da nang city',
                'username'    => 'Nguyen Phi Vu',
                'phone'       => '01225584121'
            ]
        ];

        // this is a handy shortcut
        $this->insert('accounts', $rows);
    }
}
